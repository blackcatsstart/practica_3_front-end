import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import DataTable from 'react-data-table-component';

import { useState } from 'react';
import Collapse from 'react-bootstrap/Collapse';

import Header from "./Header";
import Footer from './Footer';
import { Personas, Repuestos, duenioAuto, guardarOrden } from '../hooks/Conexion';
import { getToken } from '../utilidades/Sessionutil';
import { useForm } from 'react-hook-form';
import mensajes from '../utilidades/Mensajes';

const OrdenTrabajo = () => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [openPersona, setOpenpersona] = useState(false);
    const [openAuto, setOpenauto] = useState(false);
    const [personas, setPersonas] = useState([]);
    const [persona, setPersona] = useState([""]);
    const [cuenta, setCuenta] = useState([""]);
    const [autos, setAutos] = useState([]);
    const [auto, setAuto] = useState([""]);
    const [marca, setMarca] = useState([""]);
    const [llpersona, setLlpersona] = useState(false);
    const [isDisabled, setIsDisabled] = useState(false);
    const [isDisabledA, setIsDisabledA] = useState(false);
    const [llrepuesto, setLlrepuesto] = useState(false);
    const [repuestos, setRepuestos] = useState([]);

    const [tbldetalle, setTbldetalle] = useState([]);
    const [repuestoSelect, setrepuestoSelect] = useState({});

    const [isSelect, setIsSelect] = useState(false);
    const [eidMarca, setEidMarca] = useState("");

    const [selectedOption, setSelectedOption] = useState('');
    const [isButtonDisabled, setIsButtonDisabled] = useState(true);
    const [detalle, setDetalle] = useState([]);

    const [isButtonGuardar, setIsButtonGuardar] = useState(true);

    const [numberInput, setNumberInput] = useState('');
    const handleNumberInputChange = (event) => {
        const value = event.target.value;
        setNumberInput(value);
    };

    //var detalle = [];
    const actionBtn = () => {
        // Acceder al dato ingresado en el campo de tipo número (numberInput)
        setTbldetalle([...tbldetalle, repuestoSelect]);

        const objeto = {
            external_repuesto: selectedOption,
            cantidad: numberInput
        };
        console.log(objeto);

        setDetalle([...detalle, objeto]);
        setIsButtonGuardar(false);
        //detalle.push(objeto);
        //console.log(detalle);
        // Resto de la lógica para manejar el envío del formulario
    };

    const actionBtn2 = () => {
        var datos = {
            external_auto: auto.external_id,
            fecha_Salida: "2023-07-01",
            detalle_orden: detalle
        };

        //console.log(datos);

        guardarOrden(datos, getToken()).then((info) => {
            if (info.code != 200) {
                mensajes(info.msg, 'error', 'Error');
                //msgError(info.message);            
            } else {
                mensajes(info.msg);
            }
        }
        );

        setIsButtonGuardar(true);
    };

    const handleComboBoxChange = (event) => {
        const selectedValue = event.target.value;
        if (selectedValue !== 'default') {
            var data = JSON.parse(selectedValue);

            setrepuestoSelect(data);

            setSelectedOption(data.external_id);

        }

        checkButtonState(selectedValue);
    };


    const checkButtonState = (selectedValue) => {
        if (selectedValue !== 'default') {
            setIsButtonDisabled(false);
        } else {
            setIsButtonDisabled(true);

        }
    };


    if (!llrepuesto) {
        Repuestos(getToken()).then((info) => {
            if (info.code === 200) {
                console.log(info.info);
                setRepuestos(info.info);
            }
            setLlrepuesto(true);
        });
    }

    if (!llpersona) {
        Personas(getToken()).then((info) => {
            console.log(info);
            if (info.code === 200) {
                console.log(info.info);
                setPersonas(info.info);
            }
            setLlpersona(true);
        });
    }

    const isSelectPersona = (event) => {
        const selectedValue = event.target.value;
        if (selectedValue !== "default") {
            var data = JSON.parse(selectedValue);
            console.log(data.cuenta.correo);
            duenioAuto(data.identificacion, getToken()).then((info) => {
                var lista = info.info;
                console.log(lista);
                if (lista.length === 0) {
                    //console.log(info);
                    mensajes('No tiene Autos registrados', 'error', 'Error');
                    //msgError(info.message);            
                } else {
                    setAutos(lista);
                    setIsDisabled(!isDisabled)
                    setPersona(data);
                    setCuenta(data.cuenta.correo);
                    setOpenpersona(!openPersona);
                    mensajes(info.msg);
                }
            })
        }
    };
    const isSelectAuto = (event) => {
        const selectedValue = event.target.value;

        if (selectedValue !== "default") {
            var data = JSON.parse(selectedValue);
            console.log(data);
            setAuto(data);
            setMarca(data.marca.nombre)
            setIsDisabledA(!isDisabledA);
            setOpenauto(!openAuto);
        }
    };

    //'descripcion', 'anio', 'unidad', 'precio', 'external_id'
    const columns = [
        {
            name: 'Codigo',
            selector: row => row.external_id,
        },
        {
            name: 'Descripcion',
            selector: row => row.descripcion,
        },
        {
            name: 'Unidad',
            selector: row => row.unidad,
        },
        {
            name: 'Precio/unitario',
            selector: row => row.precio,
        },
        {
            name: 'Cantidad',
            selector: row => numberInput,
        },
        {
            name: 'Importe',
            selector: row => numberInput * row.precio,
        }
    ];



    return (
        <div>
            <Header />
            <Form>
                <Form.Group className="mb-3" controlId="formGridAddress1">
                    <h4><Form.Label>Orden de Trabajo</Form.Label></h4>
                </Form.Group>



                <Form.Group className="mb-3" controlId="formGridAddress1">
                    <Form.Label>Cliente</Form.Label>
                    <Form.Select aria-label="Default select example" disabled={isDisabled} aria-expanded={openPersona} onClick={isSelectPersona}>
                        <option value="default">Seleccione</option>
                        {personas.map((p, i) => {
                            return (<option key={i} value={JSON.stringify(p)}>
                                {p.nombres + " " + p.apellidos + " " + p.identificacion}
                            </option>)

                        })}
                    </Form.Select>

                    <Collapse in={openPersona}>
                        <div>

                            <Row className="mb-3">
                                <Col>
                                    <Form.Label>Nombre</Form.Label>
                                    <Form.Control className="nombre" defaultValue={persona.nombres} readOnly />
                                </Col>
                                <Col>
                                    <Form.Label>Apellido</Form.Label>
                                    <Form.Control placeholder="Last name" defaultValue={persona.apellidos} readOnly />
                                </Col>
                                <Col>
                                    <Form.Label>Correo</Form.Label>

                                    <Form.Control placeholder="Last name" defaultValue={cuenta} readOnly />
                                </Col>
                            </Row>

                            <Row className="mb-3">
                                <Col>
                                    <Form.Label>Direccion</Form.Label>
                                    <Form.Control placeholder="First name" defaultValue={persona.direccion} readOnly />
                                </Col>
                                <Col>
                                    <Form.Label>Identificacion</Form.Label>
                                    <Form.Control placeholder="Last name" defaultValue={persona.identificacion} readOnly />
                                </Col>
                                <Col>
                                    <Form.Label>Telefono</Form.Label>
                                    <Form.Control placeholder="Last name" defaultValue={persona.telefono} readOnly />
                                </Col>
                            </Row>

                            <Form.Group className="mb-3" controlId="formGridAddress1">
                                <Form.Label>Auto</Form.Label>
                                <Form.Select aria-label="Default select example" disabled={isDisabledA} aria-expanded={openAuto} onClick={isSelectAuto}>
                                    <option value="default">Seleccione</option>
                                    {autos.map((a, i) => {
                                        return (<option key={i} value={JSON.stringify(a)}>
                                            {a.modelo + " " + a.anio + " " + a.marca.nombre + " " + a.color}
                                        </option>)

                                    })}
                                </Form.Select>

                                <Collapse in={openAuto}>
                                    <div>
                                        <Row className="mb-3">
                                            <Col>
                                                <Form.Label>Marca</Form.Label>
                                                <Form.Control placeholder="First name" defaultValue={marca} readOnly />
                                            </Col>
                                            <Col>
                                                <Form.Label>Modelo</Form.Label>
                                                <Form.Control placeholder="Last name" defaultValue={auto.modelo} readOnly />
                                            </Col>
                                        </Row>

                                        <Row className="mb-3">
                                            <Col>
                                                <Form.Label>Año</Form.Label>
                                                <Form.Control placeholder="First name" defaultValue={auto.anio} readOnly />
                                            </Col>
                                            <Col>
                                                <Form.Label>Cilindraje</Form.Label>
                                                <Form.Control placeholder="Last name" defaultValue={auto.cilindraje} readOnly />
                                            </Col>
                                            <Col>
                                                <Form.Label>Color</Form.Label>
                                                <Form.Control placeholder="Last name" defaultValue={auto.color} readOnly />
                                            </Col>
                                        </Row>

                                        <Row className="mb-3">
                                            <Col>
                                                <Form.Label>Repuestos</Form.Label>
                                                <Form.Select aria-label="Default select example" onClick={handleComboBoxChange}>
                                                    <option value="default">Seleccione</option>
                                                    {repuestos.map((r, i) => {
                                                        return (<option key={i} value={JSON.stringify(r)}>
                                                            {r.descripcion + " " + r.unidad + " " + r.precio}
                                                        </option>)

                                                    })}
                                                </Form.Select>
                                            </Col>
                                            <Col>
                                                <Form.Label>Cantidad</Form.Label>
                                                <Form.Control type="number" value={numberInput} onChange={handleNumberInputChange} />
                                            </Col>

                                        </Row>
                                        <Form.Group className="mb-3" controlId="formGridAddress1">

                                            <hr />
                                            <Button variant="primary" disabled={isButtonDisabled} onClick={actionBtn}>
                                                Agregar
                                            </Button>
                                            <hr />
                                            <DataTable
                                                columns={columns}
                                                data={tbldetalle}
                                            />
                                            <hr />
                                            <Button variant="primary" disabled={isButtonGuardar} onClick={actionBtn2}>
                                                Guardar
                                            </Button>

                                        </Form.Group>
                                    </div>
                                </Collapse>

                            </Form.Group>
                        </div>
                    </Collapse>
                </Form.Group>
            </Form>
            <Footer />
        </div>
    );
}

export default OrdenTrabajo;