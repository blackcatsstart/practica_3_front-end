import '../css/stylea.css';
//import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./Header";
import Footer from './Footer';
import DataTable from 'react-data-table-component';
import { AutoVent, Autos } from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import EditarAuto from './EditarAuto';

const AutosVendidos = () => {
    const navegation = useNavigate();
    const [data, setData] = useState([]);
    const [llauto, setLlauto] = useState(false);

    if (!llauto) {
        AutoVent(getToken()).then((info) => {
            console.log(info);
            if (info.code === 200) {
                console.log("Entro");
                setData(info.info);
                setLlauto(true);
            }
        });
    }

    const columns = [
        {
            name: 'Modelo',
            selector: row => row.modelo,
        },
        {
            name: 'Anio',
            selector: row => row.anio,
        },
        {
            name: 'Color',
            selector: row => row.color,
        },
        {
            name: 'Nombres',
            selector: row => row.nombres,
        },
        {
            name: 'Apellidos',
            selector: row => row.apellidos,
        },
    ];



    return (

        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}
                    <div className='container-fluid'>
                        <h3><a className="dropdown-item text-center text-gray-500" href="#">Autos Vendidos</a></h3>
                        <DataTable
                            columns={columns}
                            data={data}
                        />
                    </div>
                </div>
            </div>
            <Footer />
        </div>


    );
}

export default AutosVendidos;