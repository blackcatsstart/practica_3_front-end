import '../css/stylea.css';
import { Marcas, ModificarAuto, ObtenerAuto, ObtenerColores } from '../hooks/Conexion';
import mensajes from '../utilidades/Mensajes';
import { useForm } from 'react-hook-form';
import { useState } from 'react';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import { useNavigate } from 'react-router-dom';

const EditarAuto = ({ row, onCloseModal }) => {

    console.log(row);
    const navegation = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [llauto, setLlauto] = useState(false);
    const [colores, setColores] = useState([]);
    const [marcas, setMarcas] = useState([]);
    const [llmarca, setLlmarca] = useState(false);
    const [llcolor, setLlcolor] = useState(false);


    if (!llauto) {
        ObtenerAuto(row, getToken()).then((info) => {
            Asignar(info.info);
            setLlauto(true);
        });
    }

    if (!llcolor) {
        ObtenerColores(getToken()).then((info) => {
            console.log(info);
            if (info.code === 200) {
                console.log(info.info);
                setColores(info.info);
            }
            setLlcolor(true);
        });
    }

    if (!llmarca) {
        Marcas(getToken()).then((info) => {
            console.log(info);
            if (info.code === 401 && info.msg == 'Token no valido o expirado!') {
                borrarSesion();
                mensajes(info.message);
                navegation("/sesion");
            } else {
                console.log(info.info);
                setMarcas(info.info);
                setLlmarca(true);
            }
        });
    }

    function Asignar(datos) {
        document.getElementById('modelo').value = datos.modelo;
        document.getElementById('anio').value = datos.anio;
        document.getElementById('cilindraje').value = datos.cilindraje;
        document.getElementById('precio').value = datos.precio;
    }
    //acciones
    //submit
    const onSubmit = (data) => {
        var datos = {
            "external": row,
            "external_marca": data.marca,
            "modelo": data.vane,
            "cilindraje": data.cilindraje,
            "anio": data.anio,
            "precio": data.precio,
            "color": data.color
        };

        ModificarAuto(datos, getToken()).then((info) => {
            if (info.code != 200) {
                mensajes(info.msg, 'error', 'Error');
                //msgError(info.message);            
            } else {
                mensajes(info.msg);
                onCloseModal();
            }
        }
        );

    };
    return (

        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    {/** DE AQUI CUERPO */}

                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-group">
                                        <input type="text" id='modelo' {...register('vane', { required: true })} className="form-control form-control-user" placeholder="Ingrese el modelo" />
                                        {errors.vane && errors.vane.type === 'required' && <div className='alert alert-danger'>Ingrese un modelo</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="number" id='anio' className="form-control form-control-user" placeholder="Ingrese el año" {...register('anio', { required: true })} />
                                        {errors.anio && errors.anio.type === 'required' && <div className='alert alert-danger'>Ingrese un anio</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" id='cilindraje' className="form-control form-control-user" placeholder="Ingrese el cilindraje" {...register('cilindraje', { required: true })} />
                                        {errors.cilindraje && errors.cilindraje.type === 'required' && <div className='alert alert-danger'>Ingrese un cilindraje</div>}
                                    </div>
                                    <div className="form-group">
                                        <select className='form-control' {...register('color', { required: true })}>
                                            {colores.map((aux, i) => {
                                                return (<option key={i} value={aux}>
                                                    {aux}
                                                </option>)
                                            })}
                                        </select>
                                        {errors.color && errors.color.type === 'required' && <div className='alert alert-danger'>Seleccione un color</div>}
                                    </div>
                                    {/*</div></div><div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese la placa" {...register('placa',{required:true})}/>
                                        {errors.placa && errors.placa.type === 'required' && <div className='alert alert-danger'>Ingrese una placa</div>}
                                        </div>*/}
                                    <div className="form-group">
                                        <input type="text" id='precio' className="form-control form-control-user" placeholder="Ingrese el precio" {...register('precio', { required: true, pattern: /^[0-9]*(\.[0-9]{0,2})?$/ })} />
                                        {errors.precio && errors.precio.type === 'required' && <div className='alert alert-danger'>Ingrese el precio</div>}
                                        {errors.precio && errors.precio.type === 'pattern' && <div className='alert alert-danger'>Ingrese un precio valido</div>}

                                    </div>
                                    <div className="form-group">
                                        <select className='form-control' {...register('marca', { required: true })}>
                                            {marcas.map((m, i) => {
                                                return (<option key={i} value={m.external_id}>
                                                    {m.nombre}
                                                </option>)
                                            })}
                                        </select>
                                        {errors.marca && errors.marca.type === 'required' && <div className='alert alert-danger'>Selecione una marca</div>}
                                    </div>
                                    <hr />
                                    <input className="btn btn-facebook btn-user btn-block" type='submit' value="COMPLETADO"></input>

                                </form>
                                <hr />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default EditarAuto;