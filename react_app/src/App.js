import Sesion  from './fragment/Sesion';
import Principal from './fragment/Principal';
import Inicio from './fragment/Inicio';
import ListaAuto from './fragment/ListaAuto';
import RegistroAuto from './fragment/RegistroAuto';
import AutosVendidos from './fragment/AutosVendidos';
import OrdenTrabajo from './fragment/OrdenTrabajo';
import {Routes, Route, useLocation, Navigate} from 'react-router-dom';
import { estaSesion } from './utilidades/Sessionutil';
function App() {

const Middeware = ({children}) => {
  const autenticado = estaSesion();
  const location = useLocation();
  if(autenticado) {
    return children;
  } else {
    return <Navigate to='/sesion' state={location}/>;
  }
}

const MiddewareSession = ({children}) => {
  const autenticado = estaSesion();
  const location = useLocation();
  if(autenticado) {
    return <Navigate to='/inicio'/>;
  } else {
    return children;
  }
}

  return (
    <div className="App">
      <Routes>
      <Route path='/' element={<MiddewareSession><Principal/></MiddewareSession>} exact/>
      <Route path='/sesion' element={<MiddewareSession><Sesion/></MiddewareSession>}/>
      <Route path='/inicio' element={<Middeware><Inicio/></Middeware>}/>
      <Route path='/auto' element={<Middeware><ListaAuto/></Middeware>}/>
      <Route path='/auto/vendidos' element={<Middeware><AutosVendidos/></Middeware>}/>
      <Route path='/auto/registro' element={<Middeware><RegistroAuto/></Middeware>}/>
      <Route path='/orden/registro' element={<Middeware><OrdenTrabajo/></Middeware>}/>
      </Routes>
      </div>
  );
}

export default App;
