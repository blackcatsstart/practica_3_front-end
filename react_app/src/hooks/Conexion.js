const URL = "http://localhost:3006/api";
export const InicioSesion = async (data) => {
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json"
    };
    const datos = await (await fetch(URL + "/sesion", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    console.log(datos);
    return datos;
}

export const Marcas = async (key) => {
    const cabeceras = {        
        "X-API-TOKEN": key
    };
    const datos = await (await fetch(URL + "/marcas", {
        method: "GET",
        headers: cabeceras
    })).json();    
    return datos;
}

export const MarcasCant = async (key) => {
    const cabeceras = {        
        "X-API-TOKEN": key
    };
    const datos = await (await fetch(URL + "/marcas/cantidad", {
        method: "GET",
        headers: cabeceras
    })).json();    
    return datos;
}

export const AutosCant = async (key) => {
    const cabeceras = {        
        "X-API-TOKEN": key
    };
    const datos = await (await fetch(URL + "/autos/cantidad", {
        method: "GET",
        headers: cabeceras
    })).json();    
    return datos;
}

export const Autos = async () => {
    const datos = await (await fetch(URL + "/autos", {
        method: "GET"
    })).json();
    //console.log(datos);
    return datos;
}

export const GuardarMarcas = async (data, key) => {
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json",
        "X-API-TOKEN": key
    };
    const datos = await (await fetch(URL + "/marcas/guardar", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}

export const ObtenerColores = async (key) => {
    const cabeceras = {
        "X-API-TOKEN": key        
    };
    const datos = await (await fetch(URL + "/autos/colores", {
        method: "GET",
        headers: cabeceras
    })).json();
    //console.log(datos);
    return datos;
}

export const GuardarAuto = async (data, key) => {
    console.log(data);
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json",
        "X-API-TOKEN": key        
    };
    const datos = await (await fetch(URL + "/autos/guardar", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    return datos;
}

export const ModificarAuto = async (data, key) => {
    console.log(data);
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json",
        "X-API-TOKEN": key        
    };
    const datos = await (await fetch(URL + "/autos/modificar", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    return datos;
}

export const AutoVent = async (key) => {
    const cabeceras = {
        "X-API-TOKEN": key        
    };
    const datos = await (await fetch(URL + "/autos/vendidos", {
        method: "GET",
        headers: cabeceras
    })).json();
    console.log(datos);
    return datos;
}

export const ObtenerAuto = async (data, key) => {
    const cabeceras = {
        "X-API-TOKEN": key        
    };
    const datos = await (await fetch(URL + "/autos/obtener/" + data, {
        method: "GET",
        headers: cabeceras
    })).json();
    //console.log(datos);
    return datos;
}

export const duenioAuto = async (data, key) => {
    const cabeceras = {
        "X-API-TOKEN": key        
    };
    const datos = await (await fetch(URL + "/autos/propietario/" + data, {
        method: "GET",
        headers: cabeceras
    })).json();
    console.log(datos);
    return datos;
}

export const Personas = async (key) => {
    const cabeceras = {
        "X-API-TOKEN": key        
    };
    const datos = await (await fetch(URL + "/personas", {
        method: "GET",
        headers: cabeceras
    })).json();
    //console.log(datos);
    return datos;
}

export const Repuestos = async (key) => {
    const cabeceras = {
        "X-API-TOKEN": key        
    };
    const datos = await (await fetch(URL + "/repuestos", {
        method: "GET",
        headers: cabeceras
    })).json();
    //console.log(datos);
    return datos;
}

export const guardarOrden = async (data, key) => {
    console.log(data);
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json",
        "X-API-TOKEN": key        
    };
    const datos = await (await fetch(URL + "/orden/guardar", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    return datos;
}